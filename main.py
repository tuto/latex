from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin, TestsToken

def define_env(env:PyodideMacrosPlugin):

    custom = {
                "tests": TestsToken("\n# Tests"),
                }
    env.lang.overload(custom)

    @env.macro
    def exo_pratique(titre: str, lien: str) -> str:
        """Renvoie le lien vers l'exercice

        >>> exo_pratique("Problème sympa", 'Ni/xyz-nom')
        [Problème sympa](https://e-nsi.forge.aeif.fr/pratique/Ni/xyz-nom/sujet/){ .md-button target="_blank" rel="noopener" }
        
        """
        return f'[{titre}](https://e-nsi.forge.aeif.fr/pratique/{lien}/sujet/)' + '{ .md-button target="_blank" rel="noopener" }'

    @env.macro
    def exo_ecrit(titre: str, lien: str) -> str:
        """Renvoie le lien vers l'exercice

        >>> exo_ecrit("Exercice sympa", '2020/sujet/exercice')
        [Exercice sympa](https://e-nsi.forge.aeif.fr/ecrit/2020/sujet/exercice/){ .md-button target="_blank" rel="noopener" }
        
        """
        return f'[{titre}](https://e-nsi.forge.aeif.fr/ecrit/{lien}/)' + '{ .md-button target="_blank" rel="noopener" }'


    @env.macro
    def link_ext(titre: str, lien: str, button=False) -> str:
        "le lien ne contient pas https://"
        return (
            f'[{titre}](https://{lien})'
            + '{ target="_blank" rel="noopener" '
            + (' .md-button ' if button else '')
            + ' }'
        )

    @env.macro
    def link_wp(titre: str, lien_court: str) -> str:
        return f':material-wikipedia: [{titre}](https://fr.wikipedia.org/wiki/{lien_court})' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_yt(titre: str, lien_court: str) -> str:
        return f':material-youtube: [{titre}](https://www.youtube.com/watch?v={lien_court})' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_xd(titre: str, numero: str) -> str:
        return f':octicons-comment-discussion-16: [XKCD {numero} : {titre}](https://xkcd.com/{numero}/)' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_rz(titre: str, numero: str) -> str:
        return f':fontawesome-solid-robot: [RoboZZle {numero} : {titre}](http://robozzle.com/js/play.aspx?puzzle={numero})' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_fr_ioi(titre: str, lien_court: str) -> str:
        return f'[{titre}](http://www.france-ioi.org/algo/task.php?{lien_court})' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_os(titre: str, numero: str) -> str:
        return f':octicons-number-16: [OEIS {numero} : {titre}](https://oeis.org/{numero}/)' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_doc_python(titre: str, lien_court: str) -> str:
        return f':material-language-python: [Documentation de la bibliothèque `{titre}`](https://docs.python.org/fr/3/library/{lien_court})' + '{ target="_blank" rel="noopener" }'

    @env.macro
    def link_gh(titre: str, lien_court: str) -> str:
        return f':octicons-mark-github-16: [Dépôt GitHub `{titre}`](https://github.com/{lien_court})' + '{ target="_blank" rel="noopener" }'

