---
title: Accueil
---

# Écrire des mathématiques simples

**Tutoriel** à destination des collègues enseignants ou d'élèves très motivés.

> **Prérequis**
> : Avoir déjà écrit quelques lignes en Markdown, par exemple dans un Carnet Jupyter.

![](./assets/MathJax.svg){ .autolight }
![](./assets/Jupyter.svg){width=150}
![](./assets/katex.png){width=200}
![](./assets/material.png){width=200 .autolight }


!!! abstract "Objectif"

    ![](./assets/Markdown.svg){ align="right" }

    Savoir écrire des formules mathématiques [^tour] simples dans du code Markdown [^markdown]. C'est très utile pour :
    
    - GeoGebra [^geogebra] ; écrire des informations précises sur une figure
    - Jupyter [^jupyter] ; créer un carnet mêlant code, texte et mathématiques
    - CodiMD [^codimd] ; prendre des notes scientifiques de manière collaborative
    - MkDocs [^mkdocs] ; construire un site web scientifique à l'aide de Python
    - ...

    !!! example "Exemple"

        <div class="grid" markdown>

        ```latex title="Code Markdown"
        En 1735, Leonhard Euler résout
         le **problème de Bâle** en établissant
         la formule suivante :

        $$
        \sum \limits_{k \in \mathbb N^{*}}
             \frac{1}{k^2}
        = \frac{\pi^2}{6}
        $$
    
        Cependant, il ne démontrera rigoureusement
         son résultat qu'en 1741.
        ```


        !!! abstract "Rendu"
            En 1735, Leonhard Euler résout
            le **problème de Bâle** en établissant
            la formule suivante :

            $$
            \sum \limits_{k \in \mathbb N^{*}}
                \frac{1}{k^2}
            = \frac{\pi^2}{6}
            $$
        
            Cependant, il ne démontrera rigoureusement
            son résultat qu'en 1741.
        
        </div>

        On constate que la formule mathématiques a été écrite entre des balises `$$` et qu'il s'agit de texte que l'on peut copier/coller/modifier facilement.

        C'est **la** solution professionnelle largement utilisée.

!!! danger "Humour geek"
    ![](./assets/equations.png)

    {{ link_xd('Equations', '2034') }} *All electromagnetic equations: The same as all fluid dynamics equations, but with the 8 and 23 replaced with the permittivity and permeability of free space, respectively.*


[^tour]: [Un tour d'horizon](https://www.intmath.com/cg5/katex-mathjax-comparison.php "KaTeX vs MathJax") : Comparatif KaTeX vs MathJax
[^markdown]: {{ link_wp('Markdown', 'Markdown')}} : Markdown est un langage de balisage léger créé en 2004 par John Gruber, avec l'aide d'Aaron Swartz, dans le but d'offrir une syntaxe facile à lire et à écrire en l'état dans sa forme non formatée. Markdown est principalement utilisé dans des blogs, des sites de messagerie instantanée, des forums et des pages de documentation de logiciels.
[^geogebra]: {{ link_wp('GeoGebra', 'GeoGebra')}} : GeoGebra est un logiciel interactif de géométrie, algèbre, statistique et calcul différentiel qui est conçu pour l'apprentissage de ces disciplines dans un cadre scolaire, allant du niveau primaire au niveau universitaire. GeoGebra est distribué comme logiciel libre.
[^jupyter]: {{ link_wp('Jupyter', 'Jupyter') }} : Jupyter permet de réaliser des calepins ou *notebooks*, c'est-à-dire des programmes contenant à la fois du texte, simple ou enrichi typographiquement et sémantiquement grâce au langage à balises simplifié Markdown, et du code, lignes sources et résultats d'exécution.
[^codimd]: [CodiMd dans `apps.education.fr`](https://codimd.apps.education.fr/s/H6nqsVq2y#) CodiMD est un éditeur de texte collaboratif multi-plateforme en temps réel au format MarkDown (MD). Cela signifie que vous pouvez écrire des notes avec d'autres personnes depuis un PC, une tablette ou même depuis un smartphone.
[^mkdocs]: [MkDocs](https://www.mkdocs.org/) (_en_) : _MkDocs is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation._ (Ce site est construit avec MkDocs !)
[^katex]: [KaTeX](https://katex.org/) est un moteur rapide de rendu des formules
[^mathjax]: [MathJax](https://www.mathjax.org/) est un moteur complet de rendu des formules
[^latex]: [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) est un système de composition de texte
