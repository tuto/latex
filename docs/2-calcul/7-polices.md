---
title: Écritures variées
---

# Écritures aux styles variées

!!! abstract "Écriture scripte : pour aires, volumes, cercles"
    En mode **math**ématique, on utilise, entre autres, l'écriture **scr**ipte,

    d'où la commande `#!latex $\mathscr{ }$`.

    |Markdown|Rendu|
    |--------|----:|
    |`#!latex Le cercle $\mathscr C$`|Le cercle $\mathscr C$|
    |`#!latex Le volume $\mathscr V$`|Le volume $\mathscr V$|
    |`#!latex L'aire $\mathscr A_{RST}$`|L'aire $\mathscr A_{RST}$|
    |`#!latex L'aire $\mathscr A_\text{triangle}$`|L'aire $\mathscr A_\text{triangle}$|

    Noter dans les deux derniers exemples, que
    
    - `RST` est bien en mode math, alors que
    - `\text{triangle}` produit du texte dans le mode math.
    
    C'est bien la bonne méthode.


!!! abstract "Romain droit : Pour les constantes mathématiques"
    À la différence des variables, en italique, les constantes doivent être écrites en **r**o**m**ain droit, d'où la commande `#!latex $\mathrm{ }$`.

    |Markdown|Rendu|
    |--------|----:|
    |`#!latex $\mathrm e^x$`    | $\mathrm e^x$ |
    |`#!latex $5\mathrm i - 2$` | $5\mathrm i - 2$|
    |`#!latex $\mathrm e^{\mathrm i \pi} + 1 = 0$`|$\mathrm e^{\mathrm i \pi} + 1 = 0$|

!!! abstract "Pour d'autres usages mathématiques, ou non"
    Il existe aussi :
    
    - `\mathcal` : pour l'écriture **cal**ligraphique utilisée en particulier pour écrire une structure telle qu'une courbe, un graphe, la notation de Landau, les distributions de probabilités, la topologie...
    - `\mathbb` : pour l'écriture tableau noir (_**b**lack**b**oard_) utilisée en particulier pour les ensembles de nombres.
    - `\mathfrak` : pour l'écriture gothique (**Frak**tur [^fraktur]) utilisée en algèbre, et en particulier pour l'algèbre de Lie.
    - `\mathsf` : écriture linéale, sans empattement (_**s**ans seri**f**_)
    - `\mathtt` : écriture mécane, monospace, **t**élé**t**ype


    |Markdown|Rendu|
    |--------|----:|
    |`#!latex $\mathcal O(n)$` | $\mathcal O(n)$|
    |`#!latex $\mathbb N$`    | $\mathbb N$ |
    |`#!latex L'idéal premier $\mathfrak p$`|L'idéal premier $\mathfrak p$|


    [^fraktur]: {{ link_wp("L'écriture Fraktur", 'Fraktur')}} est un type d'écriture gothique, version typographique de l'alphabet latin apparue en Allemagne au début du XVIe siècle et qui a perduré jusqu'au XXe siècle.

!!! info "Alphabet latin"
    |`\mathscr` | `\mathscr` | `\mathcal` | `\mathcal` | `\mathbb` | `\mathbb` | `\mathfrak` | `\mathfrak`|
    |--- | --- | --- | --- | --- | --- | --- | ---|
    |Scripte | scripte | Calligraphe | calligraphe | Ajourée | ajourée | Gothique | gothique|
    |$\mathscr A$ | $\mathscr a$ | $\mathcal A$ | $\mathcal a$ | $\mathbb A$ | $\mathbb a$ | $\mathfrak A$ | $\mathfrak a$|
    |$\mathscr B$ | $\mathscr b$ | $\mathcal B$ | $\mathcal b$ | $\mathbb B$ | $\mathbb b$ | $\mathfrak B$ | $\mathfrak b$|
    |$\mathscr C$ | $\mathscr c$ | $\mathcal C$ | $\mathcal c$ | $\mathbb C$ | $\mathbb c$ | $\mathfrak C$ | $\mathfrak c$|
    |$\mathscr D$ | $\mathscr d$ | $\mathcal D$ | $\mathcal d$ | $\mathbb D$ | $\mathbb d$ | $\mathfrak D$ | $\mathfrak d$|
    |$\mathscr E$ | $\mathscr e$ | $\mathcal E$ | $\mathcal e$ | $\mathbb E$ | $\mathbb e$ | $\mathfrak E$ | $\mathfrak e$|
    |$\mathscr F$ | $\mathscr f$ | $\mathcal F$ | $\mathcal f$ | $\mathbb F$ | $\mathbb f$ | $\mathfrak F$ | $\mathfrak f$|
    |$\mathscr G$ | $\mathscr g$ | $\mathcal G$ | $\mathcal g$ | $\mathbb G$ | $\mathbb g$ | $\mathfrak G$ | $\mathfrak g$|
    |$\mathscr H$ | $\mathscr h$ | $\mathcal H$ | $\mathcal h$ | $\mathbb H$ | $\mathbb h$ | $\mathfrak H$ | $\mathfrak h$|
    |$\mathscr I$ | $\mathscr i$ | $\mathcal I$ | $\mathcal i$ | $\mathbb I$ | $\mathbb i$ | $\mathfrak I$ | $\mathfrak i$|
    |$\mathscr J$ | $\mathscr j$ | $\mathcal J$ | $\mathcal j$ | $\mathbb J$ | $\mathbb j$ | $\mathfrak J$ | $\mathfrak j$|
    |$\mathscr K$ | $\mathscr k$ | $\mathcal K$ | $\mathcal k$ | $\mathbb K$ | $\mathbb k$ | $\mathfrak K$ | $\mathfrak k$|
    |$\mathscr L$ | $\mathscr l$ | $\mathcal L$ | $\mathcal l$ | $\mathbb L$ | $\mathbb l$ | $\mathfrak L$ | $\mathfrak l$|
    |$\mathscr M$ | $\mathscr m$ | $\mathcal M$ | $\mathcal m$ | $\mathbb M$ | $\mathbb m$ | $\mathfrak M$ | $\mathfrak m$|
    |$\mathscr N$ | $\mathscr n$ | $\mathcal N$ | $\mathcal n$ | $\mathbb N$ | $\mathbb n$ | $\mathfrak N$ | $\mathfrak n$|
    |$\mathscr O$ | $\mathscr o$ | $\mathcal O$ | $\mathcal o$ | $\mathbb O$ | $\mathbb o$ | $\mathfrak O$ | $\mathfrak o$|
    |$\mathscr P$ | $\mathscr p$ | $\mathcal P$ | $\mathcal p$ | $\mathbb P$ | $\mathbb p$ | $\mathfrak P$ | $\mathfrak p$|
    |$\mathscr Q$ | $\mathscr q$ | $\mathcal Q$ | $\mathcal q$ | $\mathbb Q$ | $\mathbb q$ | $\mathfrak Q$ | $\mathfrak q$|
    |$\mathscr R$ | $\mathscr r$ | $\mathcal R$ | $\mathcal r$ | $\mathbb R$ | $\mathbb r$ | $\mathfrak R$ | $\mathfrak r$|
    |$\mathscr S$ | $\mathscr s$ | $\mathcal S$ | $\mathcal s$ | $\mathbb S$ | $\mathbb s$ | $\mathfrak S$ | $\mathfrak s$|
    |$\mathscr T$ | $\mathscr t$ | $\mathcal T$ | $\mathcal t$ | $\mathbb T$ | $\mathbb t$ | $\mathfrak T$ | $\mathfrak t$|
    |$\mathscr U$ | $\mathscr u$ | $\mathcal U$ | $\mathcal u$ | $\mathbb U$ | $\mathbb u$ | $\mathfrak U$ | $\mathfrak u$|
    |$\mathscr V$ | $\mathscr v$ | $\mathcal V$ | $\mathcal v$ | $\mathbb V$ | $\mathbb v$ | $\mathfrak V$ | $\mathfrak v$|
    |$\mathscr W$ | $\mathscr w$ | $\mathcal W$ | $\mathcal w$ | $\mathbb W$ | $\mathbb w$ | $\mathfrak W$ | $\mathfrak w$|
    |$\mathscr X$ | $\mathscr x$ | $\mathcal X$ | $\mathcal x$ | $\mathbb X$ | $\mathbb x$ | $\mathfrak X$ | $\mathfrak x$|
    |$\mathscr Y$ | $\mathscr y$ | $\mathcal Y$ | $\mathcal y$ | $\mathbb Y$ | $\mathbb y$ | $\mathfrak Y$ | $\mathfrak y$|
    |$\mathscr Z$ | $\mathscr z$ | $\mathcal Z$ | $\mathcal z$ | $\mathbb Z$ | $\mathbb z$ | $\mathfrak Z$ | $\mathfrak z$|

!!! info "Chiffres"
    Pour les chiffres, scripte et calligraphie ont le même rendu.

    |`\mathrm` | `\mathsf` | `\mathtt` | `\mathscr` | `\mathbb` | `\mathfrak`|
    |--- | --- | --- | --- | --- | ---|
    |Romain | Linéale | monospace | Scripte | Ajourée | Gothique|
    |$\mathrm 0$ | $\mathsf 0$ | $\mathtt 0$ | $\mathscr 0$ | $\mathbb 0$ | $\mathfrak 0$|
    |$\mathrm 1$ | $\mathsf 1$ | $\mathtt 1$ | $\mathscr 1$ | $\mathbb 1$ | $\mathfrak 1$|
    |$\mathrm 2$ | $\mathsf 2$ | $\mathtt 2$ | $\mathscr 2$ | $\mathbb 2$ | $\mathfrak 2$|
    |$\mathrm 3$ | $\mathsf 3$ | $\mathtt 3$ | $\mathscr 3$ | $\mathbb 3$ | $\mathfrak 3$|
    |$\mathrm 4$ | $\mathsf 4$ | $\mathtt 4$ | $\mathscr 4$ | $\mathbb 4$ | $\mathfrak 4$|
    |$\mathrm 5$ | $\mathsf 5$ | $\mathtt 5$ | $\mathscr 5$ | $\mathbb 5$ | $\mathfrak 5$|
    |$\mathrm 6$ | $\mathsf 6$ | $\mathtt 6$ | $\mathscr 6$ | $\mathbb 6$ | $\mathfrak 6$|
    |$\mathrm 7$ | $\mathsf 7$ | $\mathtt 7$ | $\mathscr 7$ | $\mathbb 7$ | $\mathfrak 7$|
    |$\mathrm 8$ | $\mathsf 8$ | $\mathtt 8$ | $\mathscr 8$ | $\mathbb 8$ | $\mathfrak 8$|
    |$\mathrm 9$ | $\mathsf 9$ | $\mathtt 9$ | $\mathscr 9$ | $\mathbb 9$ | $\mathfrak 9$|

!!! info "Alphabet latin en romain"
    |`\mathit` | `\mathrm` | `\mathsf` | `\mathtt` | `\mathit` | `\mathrm` | `\mathsf` | `\mathtt`|
    |--- | --- | --- | --- | --- | --- | --- | ---|
    |Italique | Droit | Linéale | Monospace | italique | droit | linéale | monospace|
    |$\mathit A$ | $\mathrm A$ | $\mathsf A$ | $\mathtt A$ | $\mathit a$ | $\mathrm a$ | $\mathsf a$ | $\mathtt a$|
    |$\mathit B$ | $\mathrm B$ | $\mathsf B$ | $\mathtt B$ | $\mathit b$ | $\mathrm b$ | $\mathsf b$ | $\mathtt b$|
    |$\mathit C$ | $\mathrm C$ | $\mathsf C$ | $\mathtt C$ | $\mathit c$ | $\mathrm c$ | $\mathsf c$ | $\mathtt c$|
    |$\mathit D$ | $\mathrm D$ | $\mathsf D$ | $\mathtt D$ | $\mathit d$ | $\mathrm d$ | $\mathsf d$ | $\mathtt d$|
    |$\mathit E$ | $\mathrm E$ | $\mathsf E$ | $\mathtt E$ | $\mathit e$ | $\mathrm e$ | $\mathsf e$ | $\mathtt e$|
    |$\mathit F$ | $\mathrm F$ | $\mathsf F$ | $\mathtt F$ | $\mathit f$ | $\mathrm f$ | $\mathsf f$ | $\mathtt f$|
    |$\mathit G$ | $\mathrm G$ | $\mathsf G$ | $\mathtt G$ | $\mathit g$ | $\mathrm g$ | $\mathsf g$ | $\mathtt g$|
    |$\mathit H$ | $\mathrm H$ | $\mathsf H$ | $\mathtt H$ | $\mathit h$ | $\mathrm h$ | $\mathsf h$ | $\mathtt h$|
    |$\mathit I$ | $\mathrm I$ | $\mathsf I$ | $\mathtt I$ | $\mathit i$ | $\mathrm i$ | $\mathsf i$ | $\mathtt i$|
    |$\mathit J$ | $\mathrm J$ | $\mathsf J$ | $\mathtt J$ | $\mathit j$ | $\mathrm j$ | $\mathsf j$ | $\mathtt j$|
    |$\mathit K$ | $\mathrm K$ | $\mathsf K$ | $\mathtt K$ | $\mathit k$ | $\mathrm k$ | $\mathsf k$ | $\mathtt k$|
    |$\mathit L$ | $\mathrm L$ | $\mathsf L$ | $\mathtt L$ | $\mathit l$ | $\mathrm l$ | $\mathsf l$ | $\mathtt l$|
    |$\mathit M$ | $\mathrm M$ | $\mathsf M$ | $\mathtt M$ | $\mathit m$ | $\mathrm m$ | $\mathsf m$ | $\mathtt m$|
    |$\mathit N$ | $\mathrm N$ | $\mathsf N$ | $\mathtt N$ | $\mathit n$ | $\mathrm n$ | $\mathsf n$ | $\mathtt n$|
    |$\mathit O$ | $\mathrm O$ | $\mathsf O$ | $\mathtt O$ | $\mathit o$ | $\mathrm o$ | $\mathsf o$ | $\mathtt o$|
    |$\mathit P$ | $\mathrm P$ | $\mathsf P$ | $\mathtt P$ | $\mathit p$ | $\mathrm p$ | $\mathsf p$ | $\mathtt p$|
    |$\mathit Q$ | $\mathrm Q$ | $\mathsf Q$ | $\mathtt Q$ | $\mathit q$ | $\mathrm q$ | $\mathsf q$ | $\mathtt q$|
    |$\mathit R$ | $\mathrm R$ | $\mathsf R$ | $\mathtt R$ | $\mathit r$ | $\mathrm r$ | $\mathsf r$ | $\mathtt r$|
    |$\mathit S$ | $\mathrm S$ | $\mathsf S$ | $\mathtt S$ | $\mathit s$ | $\mathrm s$ | $\mathsf s$ | $\mathtt s$|
    |$\mathit T$ | $\mathrm T$ | $\mathsf T$ | $\mathtt T$ | $\mathit t$ | $\mathrm t$ | $\mathsf t$ | $\mathtt t$|
    |$\mathit U$ | $\mathrm U$ | $\mathsf U$ | $\mathtt U$ | $\mathit u$ | $\mathrm u$ | $\mathsf u$ | $\mathtt u$|
    |$\mathit V$ | $\mathrm V$ | $\mathsf V$ | $\mathtt V$ | $\mathit v$ | $\mathrm v$ | $\mathsf v$ | $\mathtt v$|
    |$\mathit W$ | $\mathrm W$ | $\mathsf W$ | $\mathtt W$ | $\mathit w$ | $\mathrm w$ | $\mathsf w$ | $\mathtt w$|
    |$\mathit X$ | $\mathrm X$ | $\mathsf X$ | $\mathtt X$ | $\mathit x$ | $\mathrm x$ | $\mathsf x$ | $\mathtt x$|
    |$\mathit Y$ | $\mathrm Y$ | $\mathsf Y$ | $\mathtt Y$ | $\mathit y$ | $\mathrm y$ | $\mathsf y$ | $\mathtt y$|
    |$\mathit Z$ | $\mathrm Z$ | $\mathsf Z$ | $\mathtt Z$ | $\mathit z$ | $\mathrm z$ | $\mathsf z$ | $\mathtt z$|
