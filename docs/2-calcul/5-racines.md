---
title: Racines
---

# Les Racines carrées

!!! abstract inline "Méthode"
    Markdown
    : `#!latex $\sqrt{x}$`
    
    Rendu
    : $\sqrt{x}$

!!! info "Remarques"
    - Pourquoi `sqrt` ?
        - En anglais _**sq**uare-**r**oo**t**_
        - `sqrt` est donc très souvent utilisé.
    - :warning: Bien penser aux accolades.
        - Sinon tout n'est pas inclus dans la racine.


!!! question "Exercice 6 : Théorème de Pythagore"
    !!! success "Un exemple classique"
        $ABC$ est un triangle rectangle en $A$, avec $AB = 45$, $AC = 28$. Calculer $BC$.

        Réponse :
        
        $ABC$ est un triangle rectangle en $A$, d'après le théorème de Pythagore, on a :
        
        $BC^2 = AB^2 + AC^2$

        $BC^2 = 45^2 + 28^2$

        $BC^2 = 2809$, avec $BC$ positif, donc

        $BC = \sqrt{2809}$

        $\boxed{BC = 53}$ ; la longueur de $BC$ est $53$.

    :+1: Pour encadrer le résultat, on peut écrire `#!latex $\boxed{BC = 53}$`.

    En vous inspirant du modèle ci-dessus, rédiger une solution au problème suivant :

    !!! note "Problème"
        $RST$ est un triangle rectangle en $R$, avec $RT = 21$, $RS=28$. Calculer $ST$.


    À vous de modifier, compléter (et réutiliser) le code suivant :

    ```latex title="Code Markdown"
    $ABC$ est un triangle rectangle en $A$, avec $AB = 45$, $AC = 28$. Calculer $BC$.

    Réponse :
    
    $ABC$ est un triangle rectangle en $A$, d'après le théorème de Pythagore, on a :
    
    $BC^2 =$
    ```

    ??? success "Réponse"
        ```latex title="Code Markdown"
        $RST$ est un triangle rectangle en $R$, avec $RT = 21$, $RS=28$. Calculer $ST$.

        Réponse :
        
        $RST$ est un triangle rectangle en $R$, d'après le théorème de Pythagore, on a :
        
        $ST^2 = RS^2 + RT^2$

        $ST^2 = 28^2 + 21^2$

        $ST^2 = 1225$, avec $ST$ positif, donc

        $ST = \sqrt{1225}$

        $\boxed{ST = 35}$ ; la longueur de $ST$ est $35$.
        ```
