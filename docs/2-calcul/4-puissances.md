---
title: Puissances
---

# Les puissances `a^n`

!!! abstract inline "L'accent circonflexe"
    Markdown
    : `#!latex $a^{n}$`
    
    Rendu
    :  $a^{n}$

!!! warning "Mode mathématique"
    Écrire en exposant de cette manière est pour le mode mathématique, entre `$`.

    L'intérieur des accolades est un contenu mathématique, pas du texte.

    Pour écrire deuxième en abrégé, on écrit `2^e^` pour 2^e^, sans entrer dans le mode mathématique.
    
    :+1: Si l'exposant est réduit à un seul caractère, les accolades ne sont pas nécessaires.


!!! example "Exemples"
    === "Correct"
        Markdown
        : `#!latex $a^{42}$`
        
        Rendu
        :  $a^{42}$

        👍 $42$ est bien mis en exposant en entier.

    === "Incorrect"
        Markdown
        : `#!latex $a^42$`
        
        Rendu
        :  $a^42$

        :warning: Seul le $4$ est mis en exposant...

!!! question "Exercice 5 : Règles et puissances"
    !!! abstract "Les règles"
        Pour tous nombres $a$, $b$, $c$ non nuls et tous entiers $n$, $m$, on a :

        $a^n \times a^m = a^{n+m}$

        $a^n \times b^n = (a \times b)^n$

        $\dfrac{a^n}{a^m} = a^{n-m}$

        $\dfrac{a^n}{b^n} = \left(\dfrac{a}{b}\right)^n$

        $\left( a^n \right)^m = a^{n \times m}$

        $a^{-n} = \dfrac {1} {a^n}$

    Copier et compléter le code Markdown ci-dessous, au sujet des règles sur les puissances.

    ```latex title="Code Markdown"
    Pour tous nombres $a$, $b$, $c$ non nuls et tous entiers $n$, $m$, on a :

    $a^n \times a^m = $

    $a^n \times b^n = $

    $\dfrac{a^n}{a^m} = $

    $\dfrac{a^n}{b^n} = $

    $\left( a^n \right)^m = $

    $a^{-n} = $
    ```

    ??? success "Réponse"
        ```latex title="Code Markdown"
        Pour tous nombres $a$, $b$, $c$ non nuls, et tous entiers $n$, $m$, on a :

        $a^n \times a^m = a^{n + m}$

        $a^n \times b^n = (a \times b)^n$

        $\dfrac{a^n}{a^m} = a^{n - m}$

        $\dfrac{a^n}{b^n} = \left( \dfrac{a}{b} \right)^n$

        $\left( a^n \right)^m = a^{n \times m}$

        $a^{-n} = \dfrac 1 {a^n}$
        ```
