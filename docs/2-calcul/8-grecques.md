---
title: Grecques
---

# Écriture de lettres grecques

!!! abstract "Par leur nom latin"
    Pour utiliser les lettres grecques, il suffit de taper leur nom en caractères latins précédé d'une contre-oblique.
    
    Par exemple, **en mode mathématique** :

    |Markdown|Rendu|
    |-------:|:----|
    |`#!latex $\alpha$` | $\alpha$ |
    |`#!latex $\chi$`   | $\chi$   |
    |`#!latex $\omega$` | $\omega$ |
    |`#!latex $\Omega$` | $\Omega$ |

    :warning: Les lettres identiques aux lettres latines ne sont pas définies :
    
    - le alpha capital est identique au $A$,
    - le khi capital est identique au $X$.
    
??? danger "Variantes de certaines lettres"
    === "Classique"
        |Markdown|Rendu|
        |-------:|:----|
        |`#!latex $\epsilon$` | $\epsilon$ |
        |`#!latex $\theta$`   | $\theta$   |
        |`#!latex $\pi$`      | $\pi$      |
        |`#!latex $\rho$`     | $\rho$     |
        |`#!latex $\sigma$`   | $\sigma$   |
        |`#!latex $\phi$`     | $\phi$     |

    === "Variante"
        |Markdown|Rendu|
        |-------:|:----|
        |`#!latex $\varepsilon$` | $\varepsilon$ |
        |`#!latex $\vartheta$`   | $\vartheta$   |
        |`#!latex $\varpi$`      | $\varpi$      |
        |`#!latex $\varrho$`     | $\varrho$     |
        |`#!latex $\varsigma$`   | $\varsigma$   |
        |`#!latex $\varphi$`     | $\varphi$     |

!!! question "Exercice 8 : Belles formules de géométrie"
    Écrire de belles formules pour :

    - Le carré
        * Le périmètre d'un carré de côté $a$.
        * L'aire d'un carré de côté $a$.
    - Le rectangle
        * Le périmètre d'un rectangle de côtés $a$ et $b$.
        * L'aire d'un rectangle de côtés $a$ et $b$.
    - Le triangle
        * Le périmètre d'un triangle de côtés $a$, $b$ et $c$.
        * L'aire d'un triangle de côté $b$ et de hauteur associée $h$.
    - Le cercle et le disque
        * La circonférence d'un cercle de rayon $r$.
        * L'aire d'un disque de rayon $r$.
    - Les volumes pour
        + Un pavé droit de côtés $L$, $H$, $P$.
        + Un prisme droit dont la base a une aire $\mathscr A_\text{base}$, et une hauteur $h$ associée.
        + Une pyramide dont la base a une aire $\mathscr A_\text{base}$, et une hauteur $h$ associée.
        + Une boule de rayon $r$.

    ??? success "Réponse"
        === "Rendu"
            - Le carré
                - Le périmètre d'un carré de côté $a$ est
                  $p_\text{carré} = 4a$.
                - L'aire d'un carré de côté $a$ est
                  $\mathscr A_\text{carré} = a^2$.
            - Le rectangle
                - Le périmètre d'un rectangle de côtés $a$ et $b$ est
                  $p_\text{rectangle} = 2(a+b)$.
                - L'aire d'un rectangle de côtés $a$ et $b$ est
                  $\mathscr A_\text{rectangle} = ab$.
            - Le triangle
                - Le périmètre d'un triangle de côtés $a$, $b$ et $c$ est
                  $p_\text{triangle} = a+b+c$.
                - L'aire d'un triangle de côté $b$ et de hauteur associée $h$ est
                  $\mathscr A_\text{triangle} = \dfrac{bh}{2}$.
            - Le cercle et le disque
                - La circonférence d'un cercle de rayon $r$ est
                  $p_\text{cercle} = 2\pi r$.
                - L'aire d'un disque de rayon $r$ est
                  $\mathscr A_\text{disque} = \pi r^2$.
            - Les volumes pour
                - Un pavé droit de côtés $L$, $H$, $P$ a pour volume
                  $\mathscr V_{pavé} = L \times H \times P$.
                - Un prisme droit dont la base a une aire $\mathscr A_\text{base}$,
                  et une hauteur $h$ associée a pour volume
                  $\mathscr V_{prisme} = \mathscr A_\text{base} \times h$.
                - Une pyramide dont la base a une aire $\mathscr A_\text{base}$,
                  et une hauteur $h$ associée a pour volume
                  $\mathscr V_{pyramide} = \dfrac {1}{3} \mathscr A_\text{base} \times h$.
                - Une boule de rayon $r$ a pour volume
                  $\mathscr V_{boule} = \dfrac{4}{3} \pi r^3$.

        === "Markdown"
            ```markdown
            - Le carré
                - Le périmètre d'un carré de côté $a$ est
                  $p_\text{carré} = 4a$.
                - L'aire d'un carré de côté $a$ est
                  $\mathscr A_\text{carré} = a^2$.
            - Le rectangle
                - Le périmètre d'un rectangle de côtés $a$ et $b$ est
                  $p_\text{rectangle} = 2(a+b)$.
                - L'aire d'un rectangle de côtés $a$ et $b$ est
                  $\mathscr A_\text{rectangle} = ab$.
            - Le triangle
                - Le périmètre d'un triangle de côtés $a$, $b$ et $c$ est
                  $p_\text{triangle} = a+b+c$.
                - L'aire d'un triangle de côté $b$ et de hauteur associée $h$ est
                  $\mathscr A_\text{triangle} = \dfrac{bh}{2}$.
            - Le cercle et le disque
                - La circonférence d'un cercle de rayon $r$ est
                  $p_\text{cercle} = 2\pi r$.
                - L'aire d'un disque de rayon $r$ est
                  $\mathscr A_\text{disque} = \pi r^2$.
            - Les volumes pour
                - Un pavé droit de côtés $L$, $H$, $P$ a pour volume
                  $\mathscr V_{pavé} = L \times H \times P$.
                - Un prisme droit dont la base a une aire $\mathscr A_\text{base}$,
                  et une hauteur $h$ associée a pour volume
                  $\mathscr V_{prisme} = \mathscr A_\text{base} \times h$.
                - Une pyramide dont la base a une aire $\mathscr A_\text{base}$,
                  et une hauteur $h$ associée a pour volume
                  $\mathscr V_{pyramide} = \dfrac {1}{3} \mathscr A_\text{base} \times h$.
                - Une boule de rayon $r$ a pour volume
                  $\mathscr V_{boule} = \dfrac{4}{3} \pi r^3$.
            ```
