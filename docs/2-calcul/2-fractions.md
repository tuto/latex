---
title: Fractions
---

# Les fractions `\dfrac`

!!! abstract "Le prototype et deux exemples"
    |Code|Résultat affiché|
    |----|---------------:|
    |`#!latex $\dfrac{num}{den}$`     | $\dfrac{num}{den}$  |
    |`#!latex $\dfrac{22}{7}$`        | $\dfrac{22}{7}$     |
    |`#!latex $5 + \dfrac{x+7}{x-1}$` | $5+\dfrac{x+7}{x-1}$|

!!! question "Exercice 3 : Règles et fractions"
    !!! info "Règles de calcul fractionnaire"
        Pour $a$, $b$, $c$ et $d$ des nombres, avec $b$ et $d$ non nuls, on a :

        $\dfrac{a}{b} + \dfrac{c}{b} = \dfrac{a + c}{b}$

        $\dfrac{a}{b} \times \dfrac{c}{d} = \dfrac{a \times c}{b \times d}$

        $a \div \dfrac{b}{d} = a \times \dfrac{d}{b}$

    Copier et compléter, dans une cellule Markdown, les règles fondamentales sur les écritures fractionnaires :

    ```latex title="Code en Markdown"
    Pour $a$, $b$, $c$ et $d$ des nombres, avec $b$ et $d$ non nuls, on a :

    $\dfrac{a}{b} + ...$

    $\dfrac{a}{b} \times ...$

    $a \div ...$
    ```

    ??? success "Réponse"

        ```latex title="Code en Markdown"
        Pour $a$, $b$, $c$ et $d$ des nombres, avec $b$ et $d$ non nuls, on a :

        $\dfrac{a}{b} + \dfrac{c}{b} = \dfrac{a + c}{b}$

        $\dfrac{a}{b} \times \dfrac{c}{d} = \dfrac{a \times c}{b \times d}$

        $a \div \dfrac{b}{d} = a \times \dfrac{d}{b}$
        ```

!!! question "Exercice 4 : Autre écriture de Milü"
    Recréer l'expression ci-dessous dans une cellule Markdown.

    $$3 + \dfrac{1}{7 + \dfrac{1}{16}} = 
    3 + \dfrac{1}{ \dfrac{7 \times 16 + 1}{16} } =
    \dfrac{3 \times 113}{113} + \dfrac{16}{113} =
    \dfrac{355}{113} \approx \pi$$

    :+1: On pourra utiliser `#!latex $\approx \pi$` pour afficher $\approx \pi$ à la fin.

    ??? success "Réponse"
    
        ```latex title="Code en Markdown"
        $3 + \dfrac{1}{7 + \dfrac{1}{16}} = 
        3 + \dfrac{1}{ \dfrac{7 \times 16 + 1}{16} } =
        \dfrac{3 \times 113}{113} + \dfrac{16}{113} =
        \dfrac{355}{113} \approx \pi$
        ```


??? danger "Pour aller plus loin - pratique non recommandée"

    !!! info inline end
        Si le contenu d'un paramètre entre accolades n'a qu'un seul caractère, les accolades ne sont pas nécessaires, on peut les remplacer par de l'espace.
        
        Si ce caractère est un chiffre, on peut même le coller à `\dfrac`, mais on ne le conseille pas, pour des raisons de lisibilité.

    |Code|Résultat|
    |----|--------:|
    |`$\dfrac12$` | $\dfrac12$ |
    |`$\dfrac1x$` | $\dfrac1x$ |
    |`$\dfrac1{x+y}$` | $\dfrac1{x+y}$ |
    |`$\dfrac{1+x}y$` | $\dfrac{1+x}y$ |
    |`$\dfrac ab$` | $\dfrac ab$ |


!!! info "Fraction plus petite"
    - `\frac` est la commande originelle pour les fractions. Elle écrit de petites fractions en mode **en ligne** et des fractions plus grandes en **mode équation**.
    - `\dfrac` est un raccourci pour `\displaystyle \frac` qui affiche de grandes fractions dans chaque situation.

    Pour ce tutoriel, on utilise partout `\dfrac` pour des raisons de lisibilité. Mais retenez qu'on utilise normalement `\frac` pour les fractions.
