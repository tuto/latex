---
title: Indices
---

# Les indices `a_n`

!!! abstract inline "Le tiret-bas"
    Markdown
    : `#!latex $a_{n}$`
    
    Rendu
    :  $a_{n}$

!!! warning "Mode mathématique"
    Écrire en indice de cette manière est pour le mode mathématique, entre `$`.

    L'intérieur des accolades est un contenu mathématique, pas du texte.

    Pour écrire du texte en indice, à l'intérieur du mode mathématique,
     on utilise `\text`, comme dans $p_{\text{carré}}$ avec `#!latex $p_{\text{carré}}$`
    
    :warning: Bien penser aux accolades.

!!! example "Exemples"
    === "Correct"
        Markdown
        : `#!latex $a_{42}$`
        
        Rendu
        :  $a_{42}$

        👍 $42$ est bien mis en indice en entier.

    === "Incorrect"
        Markdown
        : `#!latex $a_42$`
        
        Rendu
        :  $a_42$

        :warning: Seul le $4$ est mis en indice...

!!! example "Volume d'un cylindre"
    Par exemple, pour $V_\text{cylindre}$, on écrit `#!latex $V_\text{cylindre}$`.

    Nous verrons à la section suivante comment utiliser une écriture cursive pour un plus joli

    $$\mathscr V_\text{cylindre}$$

!!! question "Exercice 7 : Termes d'une suite"
    Écrire un énoncé d'exercice sur les suites, ainsi que sa réponse. Par exemple :

    !!! note "Suite de Fibonacci"
        **Énoncé**

        La suite de Fibonacci $(f_n)_n$ est définie sur l'ensemble des entiers naturels par :

        - $f_0 = 0$
        - $f_1 = 1$
        - $f_n = f_{n-1} + f_{n-2}$ pour $n > 1$

        Calculer $f_5$.

        **Réponse**

        On calcule les termes successifs :

        - $f_2 = f_1 + f_0 = 1 + 0 = 1$
        - $f_3 = f_2 + f_1 = 1 + 1 = 2$
        - $f_4 = f_3 + f_2 = 2 + 1 = 3$
        - $f_5 = f_4 + f_3 = 3 + 2 = 5$

        $\boxed{f_5 = 5}$


    ??? success 

        ```latex title="Code en Markdown"
        #### Énoncé

        La suite de Fibonacci $(f_n)_n$ est définie sur l'ensemble des entiers naturels par :

        - $f_0 = 0$
        - $f_1 = 1$
        - $f_n = f_{n-1} + f_{n-2}$ pour $n > 1$

        Calculer $f_5$.

        #### Réponse

        On calcule les termes successifs :

        - $f_2 = f_1 + f_0 = 1 + 0 = 1$
        - $f_3 = f_2 + f_1 = 1 + 1 = 2$
        - $f_4 = f_3 + f_2 = 2 + 1 = 3$
        - $f_5 = f_4 + f_3 = 3 + 2 = 5$

        $\boxed{f_5 = 5}$
        ```
