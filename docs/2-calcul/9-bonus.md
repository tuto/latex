---
title: Bonus
---

# Pour aller plus loin

- Avec ce qui a été vu précédemment, on peut écrire une large partie d'un cours de mathématiques au collège.
- Une partie de cette [section](https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_des_math%C3%A9matiques) est valable pour le mode mathématique en Markdown.
La [suite](https://fr.wikibooks.org/wiki/LaTeX/Math%C3%A9matiques) pourra aussi être utile.
    - Fonctions (trigonométriques et autres)
    - D'autres polices
    - Décorateurs (accents, vecteurs, barres, accolades horizontales, ...)
    - Symboles et quantificateurs
    - Matrices
    - ...


!!! danger "Humour geek :warning: Formules piégeuses :warning:"
    ![](./useful_geometry_formulas.png)

    {{ link_xd('Useful Geometry Formulas', '2509') }} : Geometry textbooks always try to trick you by adding decorative stripes and dotted lines.
