---
title: Parenthèses
---

# Les parenthèses à la bonne taille

!!! bug "Un problème"
    Si on veut placer un bloc (une expression) entre parenthèses, et que l'expression est plus haute que la normale (avec des fractions, par exemple), alors les parenthèses normales ne sont pas assez hautes.

    === "Parenthèses trop petites"
        Markdown
        : `#!latex $( \dfrac{a}{b} )$`

        Rendu
        : $( \dfrac{a}{b} )$

!!! abstract "Méthode"
    - On utilisera `#!latex $\left($` pour la parenthèse à gauche.
    - On utilisera `#!latex $\right)$` pour la parenthèse à droite.

    === "Parenthèses adaptées"
        Markdown
        : `#!latex $\left( \dfrac{a}{b} \right)$`
    
        Rendu
        : $\left( \dfrac{a}{b} \right)$

??? danger "Pour aller plus loin"
    - On utilisera `#!latex $\left[$` pour le crochet à gauche.
    - On utilisera `#!latex $\right]$` pour le crochet à droite.
    - On utilisera `#!latex $\left\{$` pour l'accolade à gauche. :warning: Noter qu'elle a été échappée.
    - On utilisera `#!latex $\right\}$` pour l'accolade à droite. :warning: Noter qu'elle a été échappée.

    === "Exemple avec crochets"
        Markdown
        : `#!latex $\left[ \dfrac{a}{b} \right]$`
    
        Rendu
        : $\left[ \dfrac{a}{b} \right]$

    === "Exemple avec accolades"
        Markdown
        : `#!latex $\left\{ \dfrac{a}{b} \right\}$`
    
        Rendu
        : $\left\{ \dfrac{a}{b} \right\}$

    === "Exemple complexe"
        Markdown
        : `#!latex $\left[ \left(\dfrac{a}{b} + \dfrac{c}{d} \right) \times
        \left\{  \dfrac {\dfrac{e}{f}} {\dfrac{g}{h}}   \right\}  \right]$`
    
        Rendu
        : $\left[ \left(\dfrac{a}{b} + \dfrac{c}{d} \right) \times
        \left\{  \dfrac {\dfrac{e}{f}} {\dfrac{g}{h}}   \right\}  \right]$

??? danger "Pour aller encore plus loin"
    - On utilisera `#!latex $\right.$` pour finir à droite une sélection commencée par un délimiteur à gauche.
    - On utilisera `#!latex $\left.$` pour commencer à gauche une sélection qui se finira par un délimiteur à droite.
    - On a le droit de commencer à **gauche** par une parenthèse (ou crochet ou accolade) qui **ouvre** ou qui **ferme**.
    - On a le droit de finir à **droite** par une parenthèse (ou crochet ou accolade) qui **ouvre** ou qui **ferme**.
    - Il est obligatoire d'indiquer le début et la fin de toute sélection, quitte à utiliser `#!latex $\right.$` ou `#!latex $\left.$`

    === "Exemple"
        Markdown
        : `#!latex $\left] \dfrac{a}{b} \right.$`
    
        Rendu
        : $\left] \dfrac{a}{b} \right.$
