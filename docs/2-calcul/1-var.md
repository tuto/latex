---
title: Variables
---

# Les variables

!!! abstract "Variables en mode mathématique"
    On peut bien sûr utiliser des variables mathématiques, elles seront écrites en italique, avec une police un peu différente.

!!! example "Exemple 1 : Calcul littéral"

    ```latex title="Code en Markdown"
    Pour tous nombres $k$, $a$, $b$, on a : $k(a+b) = ka + kb$
    ```

    Rendu
    : Pour tous nombres $k$, $a$, $b$, on a : $k(a+b) = ka + kb$

    :warning: Dans le rendu, comparer les lettres `a` (dans «on a», et dans «$ka+kb$»).

    :+1: C'est une bonne idée de distinguer les variables mathématiques du reste du texte.


!!! example "Exemple 2 : Volume d'un pavé"

    ```latex title="Code en Markdown"
    Le volume d'un pavé droit de longueur $L$,
    de hauteur $H$ et de profondeur $P$ est $V = L \times H \times P$
    ```
    
    Rendu
    : Le volume d'un pavé droit de longueur $L$,
     de hauteur $H$ et de profondeur $P$ est $V = L \times H \times P$

    :warning: Noter qu'il faut une espace après `\times`, sinon la commande `\timesH` est cherchée et non trouvée.

!!! tip "Nouveaux besoins"
    Si on souhaite mieux écrire une formule d'aire ou de volume, on devine la nécessité :

    - de savoir écrire des fractions,
    - de savoir écrire des parenthèses à la bonne taille,
    - de savoir écrire des puissances,
    - de savoir écrire en indice,
    - de savoir faire une écriture scripte :
    $\mathscr V$ pour volume, $\mathscr A$ pour aire, $\mathscr C$ pour cercle, ...
    - de savoir écrire quelques lettres grecques, comme $\pi$ ou $\alpha$.

    Voilà donc la suite de notre programme.
