"""
dec = ord('A') - ord('a')

tests = [
    ('mathscr', 'Scripte', 1),
    ('mathscr', 'scripte', 0),
    ('mathcal', 'Calligraphe', 1),
    ('mathcal', 'calligraphe', 0),
    ('mathbb', 'Ajourée', 1),
    ('mathbb', 'ajourée', 0),
    ('mathfrak', 'Gothique', 1),
    ('mathfrak', 'gothique', 0),
]

ligne1 = " | ".join([f"`\\{commande}`" for commande, nom, masque in tests])
ligne2 = " | ".join("---" for commande, nom, masque in tests)
ligne3 = " | ".join([f"{nom}" for commande, nom, masque in tests])

for ligne in [ligne1, ligne2, ligne3]:
    print("|" + ligne + "|")

for i in range(26):
    ligne = " | ".join([f"$\{commande} {chr(ord('a') + i + masque*dec)}$" for commande, nom, masque in tests])
    print("|" + ligne + "|")


tests = [
    ('mathrm', 'Romain'),
    ('mathsf', 'Linéale'),
    ('mathtt', 'monospace'),
    ('mathscr', 'Scripte'),
    ('mathcal', 'Calligraphie'),
    ('mathbb', 'Ajourée'),
    ('mathfrak', 'Gothique'),
]

ligne1 = " | ".join([f"`\\{commande}`" for commande, nom in tests])
ligne2 = " | ".join("---" for commande, nom in tests)
ligne3 = " | ".join([f"{nom}" for commande, nom in tests])

for ligne in [ligne1, ligne2, ligne3]:
    print("|" + ligne + "|")

for i in range(10):
    ligne = " | ".join([f"$\{commande} {chr(ord('0') + i)}$" for commande, nom in tests])
    print("|" + ligne + "|")

"""

dec = ord('A') - ord('a')

tests = [
    ('mathit', 'Italique', 1),
    ('mathrm', 'Droit', 1),
    ('mathsf', 'Linéale', 1),
    ('mathtt', 'Monospace', 1),
    ('mathit', 'italique', 0),
    ('mathrm', 'droit', 0),
    ('mathsf', 'linéale', 0),
    ('mathtt', 'monospace', 0),
]

ligne1 = " | ".join([f"`\\{commande}`" for commande, nom, masque in tests])
ligne2 = " | ".join("---" for commande, nom, masque in tests)
ligne3 = " | ".join([f"{nom}" for commande, nom, masque in tests])

for ligne in [ligne1, ligne2, ligne3]:
    print("    |" + ligne + "|")

for i in range(26):
    ligne = " | ".join([f"$\{commande} {chr(ord('a') + i + masque*dec)}$" for commande, nom, masque in tests])
    print("    |" + ligne + "|")
