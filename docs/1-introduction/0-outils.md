---
title: Outils
---

# Les outils de travail

**Objectifs**
: Être capable de faire facilement les exercices proposés sur ce site.

Il est possible d'utiliser, au choix :

- Un carnet Jupyter, par exemple avec Basthon
- Un éditeur Markdown en ligne, par exemple CodiMD
- Un éditeur de code généraliste, par exemple VSCodium
- Un logiciel de géométrie dynamique, par exemple GeoGebra
- Une application Android, par exemple Markor

!!! info "Basthon - Carnets"
    L'outil idéal pour faire les premières expériences.

    - En ligne, donc aucune installation nécessaire.
    - Respect total du RGPD, créé par et pour les enseignants.
    - Multi-plateforme : utilisable sur PC, tablette, téléphone...

    [Basthon - Carnets](https://notebook.basthon.fr/){ .md-button } Pour élèves et enseignants.

    Pour chaque cellule, il faut bien choisir le type désiré :

    - Cellule de Code Python (`Code`) ; par défaut.
    - Cellule de texte (`Markdown`) ; :warning: **il faut opérer le changement pour chaque cellule concernée**.

    :+1: Basthon est la base du système Capytale ; formidable outil pédagogique.

!!! tip "CodiMD"
    L'outil idéal pour la prise de notes collaboratives.

    - En ligne, donc aucune installation nécessaire.
    - Respect total du RGPD, créé par et pour les enseignants.
    - Multi-plateforme : utilisable sur PC, tablette, téléphone...

    -  pour les enseignants de l'Éducation Nationale
        - [CodiMd dans `apps.education.fr`](https://codimd.apps.education.fr/s/H6nqsVq2y#){ .md-button }

    - Pour les élèves ou les personnes hors Éducation Nationale, il existe d'autres versions libres avec authentification facultative. Par exemple :

        - [HedgeDoc](https://demo.hedgedoc.org/) ; on peut :
            - [Explorer les fonctionnalités](https://demo.hedgedoc.org/features)
            - Se connecter, si on a un compte. :+1: Ce n'est pas nécessaire.
            - [HedgeDoc : Créer une note en mode invité](https://demo.hedgedoc.org/new){ .md-button }
        - [StackEdit](https://stackedit.io/) : présentation du site
            - [StackEdit : L'éditeur en ligne](https://stackedit.io/app){ .md-button }

!!! success "VSCodium"
    L'outil idéal pour les créateurs de contenus textuels (codes, documents).

    - S'installe sur PC/Mac/Linux.
    - Utilisable en ligne dans la FORGE des Communs Numériques Éducatifs.
    - Avec l'extension "Markdown Preview Enhanced", on peut :
        - voir en direct le rendu à chaque touche,
        - créer une page `html` très facilement,
        - utiliser directement de nombreuses fonctionnalités : Mermaid, Admonitions, LaTeX, ...
    
    [VSCodium : présentation](https://fchambon.forge.apps.education.fr/classe/2-%C3%89diteurs/4-vscodium/){ .md-button } Pour les créateurs initiés.

!!! note "Markor"
    L'outil idéal pour prendre des notes sur tablette ou téléphone en mode hors ligne.

    - S'installe sur appareil Android.
    - Utilisable hors ligne.
    - Logiciel Open source et sans publicité.

    [Markor : présentation](https://github.com/gsantner/markor){ .md-button }
