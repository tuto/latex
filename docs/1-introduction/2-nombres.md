---
title: Nombres
---

# Écrire des nombres

On commence par donner un aperçu de la façon d'écrire un nombre, un numéro de téléphone, un nombre avec unité.

Objectif secondaire
: Comprendre que la gestion des espacements est subtile !

!!! example "Exemples avec des espaces"
    On notera l'utilisation de `~` pour obtenir une espace insécable [^insécable] en mode maths.

    [^insécable]: {{ link_wp('Espace insécable', 'Espace_ins%C3%A9cable') }} : Une espace qu'on ne coupe pas entre deux tokens. Par exemple, entre un nombre et son unité, ou dans un numéro de téléphone, ou entre un mot et un point d'exclamation, en français !

    |Markdown|Rendu|Commentaire|
    |---|---:|:---|
    |`#!latex $12 34 56 78 90$` | $12 34 56 78 90$| Les espaces sont ignorées. |
    |`#!latex $123~456~789$` | $123~456~789$| Les espaces sont insécables, ...|
    |`#!latex $12~34~56~78~90$` | $12~34~56~78~90$| ... et c'est plus lisible. |
    |`#!latex $123.456~m$`|$123.456~m$| :warning: Unité mal écrite. |
    |`#!latex $123.456~\mathrm{m}$`|$123.456~\mathrm{m}$| Méthode correcte. |
    |`#!latex $47.02~\mathrm{m}^2$`|$47.02~\mathrm{m}^2$| Méthode correcte. |

    Les deux derniers exemples seront revus plus tard.

!!! info "Espace insécable"
    - en mode mathématique :
        - `~` est une bonne idée pour le séparateur des milliers. :+1:
        - `~` est une bonne idée pour séparer un nombre et son unité. :+1:
        - L'espace insécable permet qu'un bloc ne soit pas coupé en fin de ligne. C'est important !
    - en mode texte, on utilise une autre technique. C'est aussi utile.
        - ! (_sic_) Par exemple, un point d'exclamation et son mot précédent :
            + Il est séparé par une espace insécable en **français !**
            + En anglais, il est collé. _Without any space in **English!**_


??? danger "Plus de détails sur les espaces ; :warning: technique :warning:"
    Comme dans les balises HTML, la gestion des espaces est laissée au moteur de rendu, pour chaque cas.

    Dans les balises maths, entre deux lettres ou chiffres, les espaces sont ignorées.
    
    Pour modifier l'espace que LaTeX a prévu, il y a plusieurs possibilités complexes :

    | Nom                    | Code     | taille                |
    |------------------------|----------|-----------------------|
    | l'espace fine          | `\,`     | $\frac3{18}$ de em    |
    | l'espace fine négative | `\!`     | $\frac{-3}{18}$ de em |
    | l'espace moyenne       | `\:`     | $\frac4{18}$ de em    |
    | l'espace large         | `\;`     | $\frac5{18}$ de em    |
    | le cadratin            | `\quad`  | $1$ em                |
    | le double cadratin     | `\qquad` | $2$ em                |

    **em** est une unité, la largeur de la lettre M avec la police courante.

    |Markdown|Rendu|
    |--------|----:|
    |`#!latex $41\,\text{km}$` |$41\,\text{km}$ |
    |`#!latex $35\,\text{m}^2$`|$35\,\text{m}^2$|
