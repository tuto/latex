---
title: 4 opérations
---

# Les 4 opérations

!!! success "Opérations"
    Dans une cellule Markdown, à l'intérieur de balises math,

    - L'addition s'obtient avec `+`
    - La soustraction s'obtient avec `-`
    - La multiplication s'obtient avec `\times`
    - La division s'obtient avec `\div`

??? info "Claviers du futur"
    D'ici quelque<u>s</u> ~~temps~~ années les claviers respectant la nouvelle norme AFNOR [^afnor] donneront accès rapidement à ×, ÷, et seront plus commodes pour écrire certaines lettres comme É, È, Ç, ...

    En attendant, avec Linux, il suffit
    
    - de taper sur ++altgr+";"++ pour avoir `×`
    - de taper sur ++altgr+":"++ pour avoir `÷`

    [^afnor]: [Vers une norme AFNOR pour le clavier français](https://linuxfr.org/news/vers-une-norme-afnor-pour-le-clavier-francais) (Article sur linuxfr.org)

!!! tip "LaTeX n'est pas Python"
    Dans un script Python, c'est plus facile :

    - La multiplication s'obtient avec `*`
    - La division s'obtient avec `/`

    Mais pour un document scientifique, ce n'est pas un rendu correct.

!!! example "Exemple d'opérations"
    Markdown
    : `#!latex $[5 + 3\times 8 - (1 + 35 \div 5)](18  -  5 \times 2)$`

    Rendu
    : $[5 + 3\times 8 - (1 + 35 \div 5)](18  -  5 \times 2)$

!!! question "Exercice 1 : Expression numérique"
    Rappel : [Basthon - Carnet](https://notebook.basthon.fr/) est l'outil idéal pour cet exercice.

    Exemple de palindrome
    : $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1001001)$

    1. Calculer avec une cellule de code Python l'expression numérique précédente, c'est un palindrome.
    2. Afficher cette expression avec son résultat dans une cellule Markdown.
    3. Modifier l'affichage pour insérer de l'espace inter-milliers.

    ??? success "Réponse 1."

        ```pycon title="Cellule de code Python"
        >>> 3 * 3 * 13 * 6353 * 8969 * (1 + 1480 * 1001001)
        9876543210123456789
        ```

    ??? success "Réponse 2."

        ```latex title="Cellule de code Markdown"
        $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1001001) =
        9876543210123456789$
        ```

        Rendu
        : $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1001001) =
        9876543210123456789$
        
    ??? success "Réponse 3."

        ```latex title="Cellule de code Markdown"
        $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1~001~001) =
        9~876~543~210~123~456~789$
        ```

        Rendu
        : $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1~001~001) =
        9~876~543~210~123~456~789$

        :warning: On utilise les espaces inter-milliers pour les nombres à **5 chiffres ou plus**. Pour les nombres à 4 chiffres, c'est toléré.

        ??? bug "Espace trop importante ?"
            Pour les puristes, l'espace est un peu trop importante avec `~`...

            La bonne solution est d'employer `\,` à la place.

            ```latex title="Cellule de code Markdown"
            $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1\,001\,001) =
            9\,876\,543\,210\,123\,456\,789$
            ```

            Rendu
            : $3 \times 3 \times 13 \times 6353 \times 8969 \times (1 + 1480 \times 1\,001\,001) =
            9\,876\,543\,210\,123\,456\,789$

!!! question "Exercice 2 : une approximation de $\pi$"
    Milü[^milu], un nombre fractionnaire connu ; très bonne approximation de $\pi$. [^pi]
    : $3 + 1 \div (7 + 1 \div 16)$

    1. Calculer avec une cellule Python l'expression numérique précédente.
    2. Compléter alors le code `#!latex $3 + ... \approx \pi$` pour avoir un rendu comme l'énoncé ci-dessus.
    3. (Question mathématique) Écrire Milü comme une seule fraction.
    
    [^milu]: {{ link_wp('Milü', 'Mil%C3%BC') }}, nom donné à une approximation de $\pi$ découverte par Zǔ Chōngzhī, né en 429 en Chine.
    [^pi]: {{ link_wp('un peu de lecture', 'Fraction_continue#D%C3%A9veloppement_en_fraction_continue_du_nombre_%CF%80') }} sur des approximations de $\pi$.

    ??? success "Réponse 1."

        ```pycon title="Dans une cellule de code Python"
        >>> 3 + 1 / (7 + 1/16)
        3.1415929203539825
        ```
    ??? success "Réponse 2."

        ```latex title="Dans une cellule de code Markdown"
        $3 + 1 \div (7 + 1 \div 16) \approx \pi$
        ```

        Rendu
        : $3 + 1 \div (7 + 1 \div 16) \approx \pi$

    ??? success "Réponse 3."
        
        $$\begin{align*}
        A &= 3 + 1 ÷ \left(7 + \frac{1}{16} \right)\\
        A &= 3 + 1 ÷ \left( \frac{7×16}{16} + \frac{1}{16} \right)\\
        A &= 3 + 1 ÷ \frac{112+1}{16}\\
        A &= \frac{3×113}{113} + 1 × \frac{16}{113}\\
        A &= \frac{339}{113} + 1 × \frac{16}{113}\\
        A &= \frac{339+16}{113}\\
        A &= \frac{355}{113}\\
        \end{align*}$$

