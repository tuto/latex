---
title: Balises math
---

# Les balises math `$` et `$$`

!!! tip "Où placer une formule mathématique ?"

    Il y a deux façons de placer des maths dans un document Markdown : **en ligne**, ou **en mode équation**.

    1. **En ligne** : Au milieu du texte qu'on écrit. La balise `$` de chaque côté.
        - Dans ce cas : `#!latex Du texte avant $<code math à écrire ici>$ du texte après.`
    2. **Mode équation** : Dans un paragraphe dédié, centré. La balise double `$$` de chaque côté.

        ```latex title="Code Markdown"
        Paragraphe précédent.

        $$<code math à écrire ici>$$

        Paragraphe suivant.
        ```

??? info "Autres balises"
    Le mode mathématique de MathJax (ou autre moteur de rendu) est largement employé ailleurs aussi ; avec GeoGebra, avec LaTeX et dans de nombreuses utilisations avec Markdown. Il y a des variations dans l'emploi des balises.  

    - Pour le mode en ligne, on trouve parfois, `\(<code math à écrire là>\)`.
    - Pour le mode équation, on trouve souvent `\[<code math à écrire là>\]` dans les documents LaTeX.

!!! example "Exemples"
    !!! success "1. Mode équation"
        | Markdown | Rendu |
        |:--------:|:-----:|
        | `#!latex $$x+y= y +  x$$` | $x+y= y +  x$ |

        :warning: Remarquer que l'espacement est correctement mis dans le rendu dans tous les cas.

    !!! success "2. En ligne"
        === "Incorrect"
            Markdown
            : `#!latex La somme de $123$ et 123 est égale à $123 +123$.`

            Rendu
            : La somme de $123$ et 123 est égale à $123 +123$.

        === "Correct"
            Markdown
            : `#!latex La somme de $123$ et $123$ est égale à $123 +123$.`
            
            Rendu
            : La somme de $123$ et $123$ est égale à $123 +123$.

        :warning: Remarquer les différences d'écriture 123 et $123$ dans le rendu incorrect.

??? info "Remarques"
    1. Avec le premier exemple, on voit que l'espacement dans le rendu est correct, indépendamment de la source. C'est bien ! Nous n'avons pas en nous en soucier, le moteur gère très bien !
    2. Avec le second exemple, version incorrecte, bien voir la différence d'écriture des deux premiers $123$. Dans un souci de cohérence :
        + Il vaut mieux mettre tous les nombres avec lesquels ont fait du calcul entre balises maths. Même status, même écriture !
        + Pour des années ou des numéros de page (par exemple), on peut les écrire sans balises maths. Tant qu'on ne fait pas de calcul avec ensuite.
    3. Les espaces sont gérées finement de manière globale.
        + Conseil : écrire de manière à aérer le code, qu'il soit lisible.
        + Le moteur de rendu choisira les bons espacements à afficher. C'est un principe de fonctionnement.

!!! quote "Motivation"
    L'inspiration du HTML vient de TeX, inventé par Donal Knuth. [^knuth]
    
    [^knuth]: {{ link_wp('Donald Knuth', 'Donald_Knuth') }} : un informaticien et mathématicien américain de renom, professeur émérite en informatique à l'université Stanford.

    Écrire un document en utilisant LaTeX est une nécessité pour beaucoup d'étudiants en thèse et nombre de professionnels. Ce n'est pas un apprentissage rapide [^latex], mais cela permet d'obtenir une très grande qualité.

    [^latex]: Un [cours](https://fr.wikibooks.org/wiki/LaTeX) sur la création de document avec LaTeX, c'est un gros apprentissage ; une [suite de l'apprentissage](http://www.learnlatex.org/fr/) possible.

    :+1: Utiliser Markdown, avec quelques connaissances de la balise maths LaTeX, permet de créer facilement des documents scientifiques honorables. Avec Jupyter, on peut alterner entre cellule de texte (_avec des maths_) et cellule de code (_souvent Python_). **C'est l'objectif ce cours.** :+1:

